function replaceAll(str, searchStr, replaceStr) {
  return str.split(searchStr).join(replaceStr);
}

function GenerateFilters() {
  var xpd = "xpd";
  var uupgi = "uUPGi";
  var knoResult = "kno-result";
  var g = "g";

  var urls = document.getElementById("txtUrls").value;
  urls = replaceAll(urls, "\n", " ");
  urls = urls.split(" ");

  var filterTemplate = 'google.*##.@div:has(a[href*="@url"])';

  var filterListMobile = [];
  var filterListPc = [];

  urls.forEach((url) => {
    // 모바일
    let filter = filterTemplate.replace("@url", url);
    filterListMobile.push(filter.replace("@div", xpd));
    filterListMobile.push(filter.replace("@div", uupgi));
    filterListMobile.push(filter.replace("@div", knoResult));
  });

  urls.forEach((url) => {
    // PC
    let filter = filterTemplate.replace("@url", url);
    filterListPc.push(filter.replace("@div", g));
  });

  document.getElementById("txtFiltersMobile").value = filterListMobile.join(
    "\n"
  );
  document.getElementById("txtFiltersPc").value = filterListPc.join("\n");
}

document
  .getElementById("btnGenerate")
  .addEventListener("click", (event) => GenerateFilters());
